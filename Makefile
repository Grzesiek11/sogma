all:
	g++ \
	src/main.cpp \
	-o sogma

release: release_posix release_win release_win32

release_posix:
	g++ \
	-O3 \
	-std=c++11 \
	-Ilib/json/include \
	src/main.cpp \
	-static-libgcc -static-libstdc++ \
	-o sogma

release_win:
	x86_64-w64-mingw32-g++-posix \
	-O3 \
	-std=c++11 \
	-Ilib/json/include \
	src/main.cpp \
	-static-libgcc -static-libstdc++ \
	-o sogma.exe

release_win32:
	i686-w64-mingw32-g++-posix \
	-O3 \
	-std=c++11 \
	-Ilib/json/include \
	src/main.cpp \
	-static-libgcc -static-libstdc++ \
	-o sogma32.exe