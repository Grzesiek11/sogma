#include <iostream>
#include <fstream>
#include <string>

#include <nlohmann/json.hpp>



enum OptionType {
    O_NONE,
    O_GOTO,
    O_EXIT
};



class Option {

    public:
        Option(std::string);

        virtual OptionType getType();
        std::string getDescription();
    
    private:
        std::string description;
    
};

Option::Option(std::string description) {
    this->description = description;
}

std::string Option::getDescription() {
    return description;
}

OptionType Option::getType() {
    return O_NONE;
}


class Goto : public Option {

    public:
        Goto(std::string, std::string);
        
        virtual OptionType getType();
        std::string getPage();
    
    private:
        std::string page;
    
};

Goto::Goto(std::string description, std::string page) : Option::Option(description) {
    this->page = page;
}

OptionType Goto::getType() {
    return O_GOTO;
}

std::string Goto::getPage() {
    return page;
}


class Exit : public Option {

    public:
        Exit(std::string description) : Option::Option(description) {}

        virtual OptionType getType();

};

OptionType Exit::getType() {
    return O_EXIT;
}


class Page {

    public:
        Page() {}
        Page(std::string, std::vector<Option*>);

        std::string getDescription();
        std::vector<Option*> getOptions();

        Option *display();

    private:
        std::string description;
        std::vector<Option*> options;

};

Page::Page(std::string description, std::vector<Option*> options) {
    this->description = description;
    this->options = options;
}

std::string Page::getDescription() {
    return description;
}

std::vector<Option*> Page::getOptions() {
    return options;
}

Option *Page::display() {
    std::cout << description << '\n';

    int i = 1;
    for (Option *option: options) {
        std::cout << '[' << i << "] " << option->getDescription() << '\n';
        ++i;
    }

    int choice;
    std::cin >> choice;

    if (choice - 1 >= 0 && choice - 1 < options.size()) {
        return options[choice - 1];
    } else {
        return nullptr;
    }
}



int main(int argc, char *argv[]) {
    std::string filename = "game.json";
    if (argc == 2) {
        filename = argv[1];
    }

    std::ifstream gameFile(filename);
    if (!gameFile.is_open()) {
        std::cerr << "No '" << filename << "' file.\n";
        exit(1);
    }
    nlohmann::json game;
    gameFile >> game;

    std::map<std::string, Page> pages;
    for (auto page : game["pages"].items()) {
        std::vector<Option*> options;
        for (nlohmann::json option: page.value()["options"]) {
            if (option["type"] == "goto") {
                options.push_back(new Goto(option["description"], option["page"]));
            } else if (option["type"] == "exit") {
                options.push_back(new Exit(option["description"]));
            }
        }
        pages[page.key()] = Page(page.value()["description"], options);
    }

    Page *currentPage = &pages[game["startingPage"]];
    
    bool end = 0;
    while (!end) {
        Option *option = currentPage->display();
        if (option != nullptr) {
            switch (option->getType()) {

                case O_EXIT:
                    end = 1;
                    break;
                
                case O_GOTO:
                    std::string pageId = static_cast<Goto*>(option)->getPage();
                    if (pages.find(pageId) != pages.end()) {
                        currentPage = &pages[pageId];
                    } else {
                        std::cerr << "Page '" << pageId << "' not found.\n";
                        exit(1);
                    }
                    break;

            }
        }
    }
}